#!/bin/bash
set -e

## Variables
timestamp=`date '+%Y-%m-%d_%H:%M:%S'`
config="hapee-lb.cfg"
dst="/etc/hapee-1.8"
nodes="10.2.6.31 10.2.6.32"
loadScript="load_hap_config.sh"
certScript="load_certs.sh"
updateScript="apt-update-system.sh"

## Loop to copy the load script to each node
for node in $nodes
do
  echo "Copying $loadScript to $node
  "
  scp -i ~/.ssh/id_oasadmin $loadScript oasadmin@$node:~/deploy

echo "Copying $certScript to $node
  "
  scp -i ~/.ssh/id_oasadmin $certScript oasadmin@$node:~/deploy

echo "Copying $updateScript to $node
  "
  scp -i ~/.ssh/id_oasadmin ~/scripts/$updateScript oasadmin@$node:~/

done
