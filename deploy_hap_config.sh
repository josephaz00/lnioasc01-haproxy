#!/bin/bash
set -e

## Variables
timestamp=`date '+%Y-%m-%d_%H:%M:%S'`
config="hapee-lb.cfg"
dst="/etc/hapee-1.8"
nodes="10.2.6.31 10.2.6.32"

## Functions
function validateConfig {
  /opt/hapee-1.8/sbin/hapee-lb -c -V -f /etc/hapee-1.8/hapee-lb.cfg
}
function hapStart {
  service hapee-1.8-lb start
}
function hapStop {
  service hapee-1.8-lb stop
}
function hapReload {
  service hapee-1.8-lb reload
}
function hapRestart {
  service hapee-1.8-lb restart
}
function hapStatus {
  service hapee-1.8-lb status
}

## Loop to copy the config to each node, move it into the right location, set correct permissions, validate the config, load the config, and validate that haproxy is running.
for node in $nodes
do
  echo "Pushing $config to $node
  "
  scp -i ~/.ssh/id_oasadmin $config $node:~/deploy
  
  echo "Starting deployment on $node
  "  
  ssh -t -i ~/.ssh/id_oasadmin oasadmin@$node sudo ~/deploy/load_hap_config.sh

done
