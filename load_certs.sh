#!/bin/bash
set -e

## Variables
timestamp=`date '+%Y-%m-%d_%H:%M:%S'`
cert="myrelativity_legal.pem"
src=~/deploy
dst=/etc/ssl/private

## Move cert into correct folder
  echo "Moving $cert to $dst/
  "
  cp $src/$cert $dst/

## Set new correct file permissions on the file
  echo "Setting permissions on $cert
  "
  chown root:root $dst/$cert
  chmod 664 $dst/$cert

