#!/bin/bash
set -e

## Variables
pfx="oasisdiscovery.com.pfx"
key="oasisdiscovery.com.key"
pem="oasisdiscovery.com.pem"

## Functions
function convertPfxToPem {
  #openssl pkcs12 -in $pfx -nocerts -out $key -nodes && openssl pkcs12 -in $pfx -nokeys -out $pem -nodes
  openssl pkcs12 -in $pfx -out $pem -nodes
}
## Convert the .pfx to .pem
  echo "Converting $pfx to pem format. The new file will be saved as $pem
  "
  convertPfxToPem
