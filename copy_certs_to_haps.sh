#!/bin/bash
set -e

## Variables
src="/home/oasadmin/certs"
cert="$src/myrelativity_legal.pem"
nodes="10.2.6.31 10.2.6.32"

## Loop to copy the load script to each node
for node in $nodes
do
  echo "Copying $cert to $node
  "
  scp -i ~/.ssh/id_oasadmin $cert oasadmin@$node:~/deploy

  echo "Starting cert deployment on $node
  "
  ssh -t -i ~/.ssh/id_oasadmin oasadmin@$node sudo ~/deploy/load_certs.sh

done
