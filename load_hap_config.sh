#!/bin/bash
set -e

## Variables
timestamp=`date '+%Y-%m-%d_%H:%M:%S'`
config="hapee-lb.cfg"
src=~/deploy
dst=/etc/hapee-1.8

## Functions
function validateConfig {
  /opt/hapee-1.8/sbin/hapee-lb -c -V -f /etc/hapee-1.8/hapee-lb.cfg
}
function hapStart {
  service hapee-1.8-lb start
}
function hapStop {
  service hapee-1.8-lb stop
}
function hapReload {
  service hapee-1.8-lb reload
}
function hapRestart {
  service hapee-1.8-lb restart
}
function hapStatus {
  service hapee-1.8-lb status
}
function backupHapConfig {
  cp $dst/$config $dst/backup/$config.$timestamp
}

## Backup current harpoxy config 
  echo "Backing up current $config
  "
  backupHapConfig

## Move config into correct folder
  echo "Moving $config to $dst/
  "
  cp $src/$config $dst/

## Set new correct file permissions on the file  
  echo "Setting permissions on $config
  "
  chown root:hapee $dst/$config
  chmod 644 $dst/$config

## Validate new config
  echo "Validating that $config is valid
  "
  validateConfig

## Reload the haproxy config
  echo "Reloading HAProxy 
  "
  hapReload

## Get HAProxy status
  echo "Checking  HAProxy status
  "
  hapStatus

